
---

## Create a Centos 7 machine on GCP

To create machine you need to create disk then image then machine from image as follow

- Enter GCP: (https://console.cloud.google.com/)
- Open terminal in browser or in command line (https://cloud.google.com/sdk/docs/install)
- Create disk **centos7** by running
```
gcloud compute disks create centos7 --image-project centos-cloud  --image-family centos-7 --zone europe-west4-a
```
- Create image **centos7** by running
```
gcloud compute images create centos7 \
  --source-disk centos7 --source-disk-zone europe-west4-a \
  --licenses "https://www.googleapis.com/compute/v1/projects/vm-options/global/licenses/enable-vmx"
```
- Create machine **centos-7-haswell** by running 
```
gcloud compute instances create centos-7-haswell  --machine-type=n1-standard-8   --boot-disk-size=200GB   --min-cpu-platform "Intel Haswell"   --image centos7 --zone europe-west4-a
```
---

## Set IP to be static

Go to **Exrernal IP addresses** in GCP
(https://console.cloud.google.com/networking/addresses)
And change **Type** from **Phemeral** to **Static**

---

## Install XRDP (Remote desktop service)
From terminal Connect to machine by running:
```
gcloud beta compute ssh --zone "europe-west4-a" "centos-7-haswell"
```

In machine install git, clone repo and run install script which create sudoer user **developer** with password **developer**
```
sudo yum install git -y
git clone https://boaz-betser@bitbucket.org/boaz-betser/centos-7-machine.git
cd centos-7-machine
./install_xrdp.sh
```
We recommand to change developer password after completed by running command:
```
passwd developer
```

## Connect to XRDP (Remote desktop service)
In GCP terminal run the following to get external ip address:
```
gcloud compute addresses list
```

Then run in windows:
```
mstsc /v:<external ip address> /f
```
