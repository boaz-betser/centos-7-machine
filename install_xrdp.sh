#!/bin/bash
sudo yum install git -y
sudo adduser developer
sudo usermod -aG wheel developer
echo "developer" | passwd developer --stdin
sudo rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-1.el7.nux.noarch.rpm
sudo yum groupinstall "GNOME Desktop" "Graphical Administration Tools" -y
sudo ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target
sudo yum -y install xrdp tigervnc-server
sudo systemctl start xrdp.service
sudo netstat -antup | grep xrdp
sudo systemctl enable xrdp.service
sudo yum update -y

