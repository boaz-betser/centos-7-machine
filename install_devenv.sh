#!/bin/bash
echo "Installing vs code"
sudo yum remove /home/developer/Downloads/code-1.52.1-1608137084.el7.x86_64.rpm 
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo echo "[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc" > vscode.repo
sudo cp vscode.repo /etc/yum.repos.d/vscode.repo
sudo yum install code -y


